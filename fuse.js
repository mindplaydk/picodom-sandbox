const { FuseBox, BabelPlugin } = require("fuse-box");

const fuse = FuseBox.init({
    homeDir: "src",
    output: "dist/$name.js",
    plugins: [
        BabelPlugin({
          config: {
            sourceMaps: true,
            presets: ["es2015"],
            plugins: [
                ["transform-react-jsx"],
            ],
          },
        }),
    ],
});

fuse.bundle("app")
    .sourceMaps(true)
    .instructions(`>index.tsx`);

fuse.run();
