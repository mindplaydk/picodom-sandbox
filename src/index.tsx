var { h, patch } = picodom
/** @jsx h */

function bind(view, node) {
  let old;
  return (...state) => patch(old, old = view(...state), node)
}

let render = bind(view, document.getElementById("app"));

function view(state) {
  return (
    <div>
      <h1>{state}</h1>
      <input
        autofocus
        type="text"
        value={state}
        oninput={e => render(e.target.value)}
      />
    </div>
  )
}

render("Hello!");
